package com.example.nelsoncoelho.finalproject;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ExampleListActivity extends ListActivity
{
    //String[] names, details;
    ArrayList<String> names  = new ArrayList<String>();
    ArrayList<String> details  = new ArrayList<String>();
    MyHelper db = new MyHelper(this);
    double Northing = 0;
    double Easting = 0;


    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String search = "";
        boolean towns = prefs.getBoolean("towns", true);
        boolean cities = prefs.getBoolean("cities", true);
        boolean pubs = prefs.getBoolean("pubs", true);
        boolean restaurants = prefs.getBoolean("restaurants", true);
        boolean hills = prefs.getBoolean("hills", true);
        boolean memorials = prefs.getBoolean("memorials", true);
        boolean sports = prefs.getBoolean("sports", true);
        boolean fountains = prefs.getBoolean("fountains", true);
        boolean libraries = prefs.getBoolean("libraries", true);
        boolean universities = prefs.getBoolean("universities", true);
        boolean legals = prefs.getBoolean("legals", true);
        boolean gardens = prefs.getBoolean("gardens", true);

        if (towns) search += "type='town' OR ";
        if (cities) search += "type='city' OR ";
        if (pubs) search += "type='pub' OR ";
        if (restaurants) search += "type='restaurant' OR ";
        if (hills) search += "type='hill' OR ";
        if (memorials) search += "type='memorial' OR ";
        if (sports) search += "type='sport' OR ";
        if (fountains) search += "type='fountain' OR ";
        if (libraries) search += "type='library' OR ";
        if (universities) search += "type='university' OR ";
        if (legals) search += "type='legal' OR ";
        if (gardens) search += "type='garden' OR ";

        search = search.substring(0, search.length() - 3);
        System.out.println(search);

        String new_easting = getIntent().getStringExtra("Easting");
        String new_northing = getIntent().getStringExtra("Northing");
        Northing = Double.parseDouble(new_northing);
        Easting = Double.parseDouble(new_easting);
        //ArrayList<POIDB> found = db.findAll();
        ArrayList<POIDB> found = db.findMultiple(search);
        System.out.println("RESULT FOUND: "+found);
        int i = 0;
        for (i = 0; i < found.size(); i++) {
            try {
                POIDB row = found.get(i);
                //System.out.println(row.name+"("+row.type+")");
                names.add(row.name+"("+row.type+")");
                double distance = Math.sqrt(Math.pow((double) row.northing-Northing, 2) + Math.pow((double) row.easting-Easting, 2));
                details.add(row.description+" (Distance: "+distance+" meters)");
                //nPointsOfInterest++;
                //System.out.println(nPointsOfInterest);
                //PointsOfInterest = new POIDB(ID, name, type, country, region, description, lon, lat, easting, northing);
                //long new_id = db.insertRecord(ID, name, type, country, region, description, lon, lat, northing, easting);

                //i++;
            } catch (IndexOutOfBoundsException e) {
                System.out.println(e.toString());
            }
        }

        MyAdapter adapter = new MyAdapter();
        setListAdapter(adapter);
    }

    /*
    The ListView holding all the list items (which normally you do not need to deal with directly if you are using a ListActivity);
    The View representing the individual list item being selected;
    The index of the selected item within the ListView;
    and the item ID (as specified in the XML) of the selected item, if it has one.
     */
    public void onListItemClick(ListView lv, View view, int index, long id)
    {
        // handle list item selection
    }

    public class MyAdapter extends ArrayAdapter<String>
    {
        public MyAdapter()
        {
            // We have to use ExampleListActivity.this to refer to the outer class (the activity)
            super(ExampleListActivity.this, android.R.layout.simple_list_item_1, names);
        }

        public View getView(int index, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if(view==null)
            {
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.poientry, parent, false);
            }
            TextView title = (TextView)view.findViewById(R.id.poi_name), detail =
                    (TextView)view.findViewById(R.id.poi_desc);
            title.setText(names.get(index));
            detail.setText(details.get(index));
            return view;
        }
    }
}