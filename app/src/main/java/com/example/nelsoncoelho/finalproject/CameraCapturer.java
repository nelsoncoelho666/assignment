package com.example.nelsoncoelho.finalproject;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;

import java.io.IOException;

/**
 * Created by nelsoncoelho on 21/02/2017.
 */

public class CameraCapturer {
    Camera camera;

    public void openCamera() {

        try {
            camera = Camera.open();
        }
        catch (Exception e) {
            // catch exceptions here ideally...
        }
    }

    public void startPreview(SurfaceTexture surfaceTexture) throws IOException
    {
        camera.setPreviewTexture(surfaceTexture);
        camera.startPreview();
    }

    public void stopPreview() {
        camera.stopPreview();
        releaseCamera();
    }

    public void releaseCamera() {
        if(camera!=null) {
            camera.release();
            camera=null;
        }
    }

    boolean isActive()
    {
        return camera!=null;
    }
}
