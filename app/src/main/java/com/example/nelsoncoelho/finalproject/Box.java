package com.example.nelsoncoelho.finalproject;

import android.opengl.Matrix;
import android.view.MotionEvent;
import android.view.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by nelsoncoelho on 07/02/2017.
 */

public class Box {
    ShortBuffer indexbuffer;
    FloatBuffer vbuf;
    FloatBuffer colourbuffer;
    float x, y, z;
    String type;
    public Box(float x, float y, float z, String type, GPUInterface gpui)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        float[] vertices = new float[]{x, y, z,
                x + 0.5f, y, z,
                x + 0.5f, y + 0.5f, z,
                x, y + 0.5f, z,
                x, y, z - 0.5f,
                x + 0.5f, y, z - 0.5f,
                x + 0.5f, y + 0.5f, z - 0.5f,
                x, y + 0.5f, z - 0.5f};
        ByteBuffer vbuf0 = ByteBuffer.allocateDirect(vertices.length * Float.SIZE);
        vbuf0.order(ByteOrder.nativeOrder());
        vbuf = vbuf0.asFloatBuffer();
        vbuf.put(vertices);
        vbuf.position(0);

        short[] indices = { 0,1,2, 0,2,3, 1,5,6, 1,6,2, 5,4,7, 5,7,6, 4,0,3, 4,3,7, 4,5,1, 4,1,0, 3,2,6, 3,6,7 };
        ByteBuffer ibuf = ByteBuffer.allocateDirect(indices.length*Short.SIZE);
        ibuf.order(ByteOrder.nativeOrder());
        indexbuffer = ibuf.asShortBuffer();
        indexbuffer.put(indices);
        indexbuffer.position(0);

        //float[] colour = {0.0f,0.0f,1.0f, 0.0f,0.0f,1.0f, 1.0f,0.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,1.0f, 1.0f,0.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,1.0f, 1.0f,0.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,1.0f, 1.0f,0.0f,0.0f, 1.0f,0.0f,0.0f};
        float[] colour;

        switch(type) {
            case "town":
                colour = new float[]{1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f};
                break;
            case "city":
                colour = new float[]{0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
                break;
            case "pub":
                colour = new float[]{0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};
                break;
            case "restaurant":
                colour = new float[]{0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f};
                break;
            case "hills":
                colour = new float[]{1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f};
                break;
            case "memorials":
                colour = new float[]{1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f};
                break;
            default:
                colour = new float[]{0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f};
                break;

        }

        ByteBuffer cbuf = ByteBuffer.allocateDirect(indices.length*Short.SIZE);
        cbuf.order(ByteOrder.nativeOrder());
        colourbuffer = cbuf.asFloatBuffer();
        colourbuffer.put(colour);
        colourbuffer.position(0);

    }


    public void render(GPUInterface gpui)
    {

        //gpui.setUniform4fv("uColour", new float[]{1.0f, 0.0f, 0.0f, 1.0f});
        //gpui.drawBufferedData(vbuf, 0, "aVertex", 0, 8);
        gpui.drawColouredBufferedData(vbuf, 0, "aVertex", indexbuffer, colourbuffer, "aColour");
    }


}
