package com.example.nelsoncoelho.finalproject;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by nelsoncoelho on 10/01/2017.
 */

public class GPUInterface {
    int vertexShade, fragmentShade, shadeProgram;
    public GPUInterface ()
    {

    }

    public GPUInterface (String texVertexShader, String texFragmentShader)
    {
        vertexShade = getShader(GLES20.GL_VERTEX_SHADER, texVertexShader);
        fragmentShade = getShader(GLES20.GL_FRAGMENT_SHADER, texFragmentShader);
        shadeProgram = makeProgram(vertexShade, fragmentShade);
    }

    public int getShader(int shaderType, String shaderSource)
    {
        int shader = GLES20.glCreateShader(shaderType);
        GLES20.glShaderSource(shader, shaderSource);
        GLES20.glCompileShader(shader);

        int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(shader,GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if(compileStatus[0]==0){
            Log.e("OpenGL", "Error compiling shader: " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader);
            shader = -1;
        }

        return shader;
    }

    public int makeProgram(int vertexShader, int fragmentShader)
    {
        int shaderProgram=GLES20.glCreateProgram();
        GLES20.glAttachShader(shaderProgram, vertexShader);
        GLES20.glAttachShader(shaderProgram, fragmentShader);
        GLES20.glLinkProgram(shaderProgram);
        int[] linkStatus = new int[1];
        GLES20.glGetProgramiv(shaderProgram, GLES20.GL_LINK_STATUS, linkStatus, 0);
        if(linkStatus[0]==0){
            Log.e("OpenGL", "Error linking shader program: " + GLES20.glGetProgramInfoLog(shaderProgram));
            GLES20.glDeleteProgram(shaderProgram);
            shaderProgram = -1;
        }
        else {
            GLES20.glUseProgram(shaderProgram);
        }
        this.shadeProgram = shaderProgram;
        return shaderProgram;
    }

    public int getShaderAttribVarRef(String shaderAttribute)
    {
        return GLES20.glGetAttribLocation(shadeProgram, shaderAttribute);
    }

    public void setUniform4fv(String shaderVariable, float[] data)
    {
        int location = GLES20.glGetUniformLocation(shadeProgram, shaderVariable);
        GLES20.glUniform4fv(location, 1, data, 0);
    }

    public void drawBufferedData(FloatBuffer vertexBuffer, int stride, String attributeVariable, int firstVertex, int verticesNumber)
    {
        // Create a reference to the attribute variable aVertex
        int ref_aVertex = GLES20.glGetAttribLocation(shadeProgram, attributeVariable);

        // Enable it
        GLES20.glEnableVertexAttribArray(ref_aVertex);

        //Tells the GPU the format of the data
        GLES20.glVertexAttribPointer(ref_aVertex, 3, GLES20.GL_FLOAT, false, stride, vertexBuffer);

        //Draws the shape
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, firstVertex, verticesNumber);


    }

    public void  drawColouredBufferedData(FloatBuffer vertexBuffer, int stride, String attributeVariable, ShortBuffer indexBuffer, FloatBuffer colourBuffer, String attributeColorVariable)
    {
        int attrVarRef= getShaderAttribVarRef(attributeVariable);
        int colourVarRef =  getShaderAttribVarRef(attributeColorVariable);
        vertexBuffer.position(0);
        indexBuffer.position(0);
        colourBuffer.position(0);

        GLES20.glEnableVertexAttribArray(attrVarRef);
        GLES20.glVertexAttribPointer(attrVarRef, 3, GLES20.GL_FLOAT, false, stride, vertexBuffer);

        GLES20.glEnableVertexAttribArray(colourVarRef);
        GLES20.glVertexAttribPointer(colourVarRef, 3, GLES20.GL_FLOAT, false, stride, colourBuffer);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexBuffer.limit(), GLES20.GL_UNSIGNED_SHORT, indexBuffer);
    }

    public void setUniform1I (String shaderName, int shaderValue)
    {
        int refShaderVar = GLES20.glGetUniformLocation(shadeProgram, shaderName);
        GLES20.glUniform1i(refShaderVar, shaderValue);
    }
    public void sendMatrix(String shaderVariable, float[] matrix)
    {
        int ref_uModelviewMatrix = GLES20.glGetUniformLocation(shadeProgram, shaderVariable);
        GLES20.glUniformMatrix4fv (ref_uModelviewMatrix, 1, false, matrix, 0);
    }

    public void select()
    {
        GLES20.glUseProgram(shadeProgram);
    }


}
