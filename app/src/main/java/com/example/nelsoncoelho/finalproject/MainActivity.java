package com.example.nelsoncoelho.finalproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends Activity implements SensorEventListener, LocationListener {
    OpenGLView openglview;

    float[] accelerometerValues = new float[3];
    float[] magneticFieldValues = new float[3];
    float[] orientations = new float[3];
    float[] remappedMatrix = new float[16];
    public static volatile float k = (float)0.05;
    Sensor accel;
    Sensor magn;
    float test = 0;
    POIDB PointsOfInterest;
    int nPointsOfInterest = 0;
    Point Location;
    MyHelper db = new MyHelper(this);

    //ArrayList<Box> PointsOfInterestList  = new ArrayList<Box>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_main);
        openglview = new OpenGLView(this);
        //setContentView(openglview);

        LocationManager mgr=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
        mgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);

        SensorManager sMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accel = sMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magn = sMgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sMgr.registerListener(this,accel, SensorManager.SENSOR_DELAY_UI);
        sMgr.registerListener(this,magn, SensorManager.SENSOR_DELAY_UI);

        //sMgr = (SensorManager)getSystemService(SENSOR_SERVICE);

        //openglview.newPOI(441712, 0, 0);
        openglview.newPOI(0, 0, -5, "city");

    }

    public void onStart()
    {

        super.onStart();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        double latitude =  Double.parseDouble(prefs.getString("lat", "50.9"));
        double longitude =  Double.parseDouble(prefs.getString("lon", "-1.4"));
        int zoom =  Integer.parseInt(prefs.getString("zoom", "14"));
        //      int zoom =14;

        String mapstyle = prefs.getString("mapstyle", "Regular");
        Log.d("mapping","map style" + mapstyle);
        Boolean tracking = prefs.getBoolean("tracking", true);

        /*Toast.makeText
                (this, "Location=" +
                        latitude+ " " +
                        longitude , Toast.LENGTH_LONG).show();*/
        //mv.getController().setCenter(new GeoPoint(latitude,longitude));

        //mv.getController().setZoom(zoom);

        /*if(tracking == true) {
            tracking = false;
            mgr.removeUpdates(this);

        }
        else {
            tracking = true;
            mgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }*/

        if(mapstyle.equals("Cycle"))
        {
            //mv.getTileProvider().setTileSource(TileSourceFactory.CYCLEMAP);
        }
        else
        {
            //mv.getTileProvider().setTileSource(TileSourceFactory.MAPNIK);
        }

        Long ID = Long.valueOf(1);
        String name = "Home";
        String type = "City";
        String country= "England";
        String region = "Southampton";
        String description = "description";
        Double lon = 0.0;
        Double lat = 0.0;
        Double easting = 0.0;

        long new_id = 0;
        Double northing = 0.0;

        northing = 441705.0;
        //new_id = db.insertRecord(ID, name, type, country, region, description, lon, lat, northing, easting);

        ID = Long.valueOf(2);
        northing = 441712.0;
        //new_id = db.insertRecord(ID, name, type, country, region, description, lon, lat, northing, easting);

    }

    class MyTask extends AsyncTask<String,Void,String>
    {
        public String doInBackground(String... artist)
        {
            HttpURLConnection conn = null;
            try
            {
                //URL url = new URL("http://www.free-map.org.uk/course/mad/ws/hits.php?artist="+artist[0]+"&format=json");
                double minx, miny, maxx, maxy;
                minx = Location.x-1000;
                miny = Location.y-1000;
                maxx = Location.x+1000;
                maxy = Location.y+1000;
                URL url = new URL("http://www.free-map.org.uk/course/msc/ws/poi.php?bbox=" + Double.toString(minx) + ","+Double.toString(miny)+","+Double.toString(maxx)+","+ Double.toString(maxy));
                conn = (HttpURLConnection) url.openConnection();
                InputStream in = conn.getInputStream();
                if(conn.getResponseCode() == 200)
                {
                    nPointsOfInterest++;
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String result = "", line;
                    while((line = br.readLine()) !=null)
                        result += line;

                    return result;

                }
                else
                    return "HTTP ERROR: " + conn.getResponseCode();


            }
            catch(IOException e)
            {
                return e.toString();
            }
            finally
            {
                if(conn!=null)
                    conn.disconnect();
            }
        }

        public void onPostExecute(String result)
        {
            //EditText et1 = (EditText)findViewById(R.id.et1);
            //et1.setText(result);
            try
            {
                JSONArray jsonArr = new JSONArray (result);
                String text;
                //text = "";
                int beginRow = 0;
                for(int i=0; i<jsonArr.length(); i++)
                {
                    JSONObject curObj = jsonArr.getJSONObject(i);
                    Long ID = curObj.getLong("ID");
                    String name = curObj.getString("name");
                    String type = curObj.getString("type");
                    String country= curObj.getString("country");
                    String region = curObj.getString("region");
                    String description = curObj.getString("description");
                    Double lon = curObj.getDouble("lon");
                    Double lat = curObj.getDouble("lat");
                    Double easting = curObj.getDouble("easting");
                    Double northing = curObj.getDouble("northing");

                    POIDB found = db.findId(ID.toString());
                    if(found == null) {
                        nPointsOfInterest++;
                        long new_id = db.insertRecord(ID, name, type, country, region, description, lon, lat, northing, easting);
                        openglview.newPOI(northing.floatValue(), easting.floatValue(), 0, type);
                    }
                    //text +=" ID= "+ ID + " Name = " + name + " Type = " + type + " Country = " + country + " Region = " + region + " Lon = " + lon + " Lat = " + lat + " Easting = " + easting +" Northing = " + northing + "\n";
                }


                //TextView tv = (TextView) findViewById((R.id.tv));
                //tv.setText(text);
                // tv.setText(result);

            }
            catch (JSONException e)
            {
                new AlertDialog.Builder(MainActivity.this).setMessage(e.toString()).setPositiveButton("OK", null).show();
            }
        }
    }

    public void onLocationChanged(Location newLoc)
    {
        OpenGLView openglnew = (OpenGLView) findViewById(R.id.openglview);
        Toast.makeText
                (this, "Location=" +
                        newLoc.getLatitude()+ " " +
                        newLoc.getLongitude() , Toast.LENGTH_LONG).show();

        OSGBProjection proj = new OSGBProjection();
        //System.out.println(proj.unproject(new Point(489600,128500)));
        //System.out.println(proj.project(new Point(-0.72,51.05)));

        openglnew.translate(proj.project(new Point(newLoc.getLongitude(), newLoc.getLatitude())));
        Location = proj.project(new Point(newLoc.getLongitude(), newLoc.getLatitude()));
        System.out.println("LOCATION: "+Location);
        System.out.println("Found: "+nPointsOfInterest);


        TextView tv_lon = (TextView) findViewById(R.id.lon);
        TextView tv_lat = (TextView) findViewById(R.id.lat);

        tv_lon.setText("Northing: "+Location.x);
        tv_lat.setText("Easting: "+Location.y);



        /*TextView tv2 = (TextView)findViewById(R.id.tv2);
        String latitude = String.valueOf(newLoc.getLatitude());
        tv2.setText(latitude+"\n");

        TextView tv4 = (TextView)findViewById(R.id.tv4);
        String longitude = String.valueOf(newLoc.getLongitude());
        tv4.setText(longitude);*/

        MainActivity.MyTask t = new MainActivity.MyTask();
        t.execute();

    }

    public void onProviderDisabled(String provider)
    {
        Toast.makeText(this, "Provider " + provider +
                " disabled", Toast.LENGTH_LONG).show();
    }

    public void onProviderEnabled(String provider)
    {
        Toast.makeText(this, "Provider " + provider +
                " enabled", Toast.LENGTH_LONG).show();
    }

    public void onStatusChanged(String provider,int status,Bundle extras)
    {

        Toast.makeText(this, "Status changed: " + status,
                Toast.LENGTH_LONG).show();
    }





    public void onSensorChanged(SensorEvent event) {
        TextView tv_x = (TextView) findViewById(R.id.x);
        TextView tv_y = (TextView) findViewById(R.id.y);
        TextView tv_z = (TextView) findViewById(R.id.z);


        if (event.sensor == accel) {
            for (int i = 0; i < 3; i++)
                //accelerometerValues[i] = event.values[i];
                accelerometerValues[i] = (float) ((event.values[i] * k) + (accelerometerValues[i] * (1.0 - k))); //SMOOTHERED WAY
        } else {
            for (int i = 0; i < 3; i++)
                //magneticFieldValues[i] = event.values[i];
                magneticFieldValues[i] = (float) ((event.values[i] * k) + (magneticFieldValues[i] * (1.0 - k)));
            //event.sensor = accelerometerValues[];
        }

        //float stored_value = raw_value*k + previous_value*(1-k)


        float[] orientationMatrix = new float[16];
        //float[] inclinationMatrix = null;


        SensorManager.getRotationMatrix(orientationMatrix, null, accelerometerValues, magneticFieldValues);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            SensorManager.remapCoordinateSystem(orientationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, remappedMatrix);
            SensorManager.getOrientation(remappedMatrix, orientations);
        }
        else
        {
            SensorManager.getOrientation(orientationMatrix, orientations);
        }

        tv_x.setText(String.valueOf(orientations[0] * 180 / Math.PI));
        tv_y.setText(String.valueOf(orientations[1] * 180 / Math.PI));
        tv_z.setText(String.valueOf(orientations[2] * 180 / Math.PI));

        OpenGLView openglnew = (OpenGLView) findViewById(R.id.openglview);
        float[] rotation = {(float) (orientations[0] * 180 / Math.PI), (float) (orientations[1] * 180 / Math.PI) + 80, (float) (orientations[2] * 180 / Math.PI)};
        //float[] rotation = SensorManager.getOrientation(remappedMatrix, orientations);
        //if(test < 180)
        //for(int i = 0; i < 2; i++)
          //  orientations[i] *= 180 / Math.PI;
        //openglnew.rotate(orientations);

        //SENSORS ROTATION
        openglnew.rotate(rotation);
        //openglnew.setRotationMatrix(rotation);


    }



    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.menu1)
        {
            Intent intent = new Intent(this,PreferencesActivity.class);
            startActivity(intent);
            return true;
        }
        else if(item.getItemId() == R.id.menu2)
        {
            // react to the menu item being selected...
            if(Location != null) {
                Bundle localBundle = new Bundle();
                Intent intent = new Intent(this, ExampleListActivity.class);
                intent.putExtra("Northing", Double.toString(Location.x));
                intent.putExtra("Easting", Double.toString(Location.y));
                startActivity(intent);
            }
            else
                Toast.makeText
                        (this, "Location not detected.", Toast.LENGTH_LONG).show();
            return true;
        }
        else if(item.getItemId() == R.id.menu3)
        {
            // react to the menu item being selected...
            db.deleteAllRows();
            MainActivity.MyTask t = new MainActivity.MyTask();
            t.execute();
            Toast.makeText
                    (this, "Database deleted, acquiring new points of interest..." , Toast.LENGTH_LONG).show();
            //Intent intent = new Intent(this,DownloadActivity.class);
            //startActivity(intent);
            return true;
        }

        return false;
    }

    protected void onActivityResult(int requestCode,int resultCode,Intent intent)
    {
        if(requestCode==0)
        {
            if (resultCode==RESULT_OK)
            {
                //Bundle is a set of keys and values
                Bundle extras=intent.getExtras();
                boolean cycle = extras.getBoolean("com.example.cycle");
                if(cycle==true)
                {
                    //mv.getTileProvider().setTileSource(TileSourceFactory.CYCLEMAP);
                }
                else
                {
                    //mv.getTileProvider().setTileSource(TileSourceFactory.MAPNIK);
                }
            }
        }

    }

    public void onResume()
    {
        super.onResume();
        openglview.onResume();
    }

    public void onPause()
    {
        super.onPause();
        //openglview.onPause();
    }
}
