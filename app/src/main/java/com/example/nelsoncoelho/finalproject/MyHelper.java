package com.example.nelsoncoelho.finalproject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

/**
 * Created by nelsoncoelho on 07/03/2017.
 */

public class MyHelper extends SQLiteOpenHelper {
    static final int VERSION = 2;
    static final String DATABASE_NAME = "TestDB";

    public MyHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, VERSION);
    }

    public void onCreate(SQLiteDatabase db) {

        db.execSQL ("CREATE TABLE IF NOT EXISTS POIDB (Id INTEGER PRIMARY KEY, name VARCHAR(255), type VARCHAR(255), country VARCHAR(255),  region VARCHAR(255), description VARCHAR(255), lon DOUBLE, lat DOUBLE, northing DOUBLE, easting DOUBLE)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL ("DROP TABLE IF EXISTS POIDB");
        onCreate(db);
    }
    public void deleteAllRows()
    {
        SQLiteDatabase db = getReadableDatabase();
        db.execSQL ("DROP TABLE IF EXISTS POIDB");
        db.execSQL ("CREATE TABLE IF NOT EXISTS POIDB (Id INTEGER PRIMARY KEY, name VARCHAR(255), type VARCHAR(255), country VARCHAR(255),  region VARCHAR(255), description VARCHAR(255), lon DOUBLE, lat DOUBLE, northing DOUBLE, easting DOUBLE)");
    }



    public ArrayList<POIDB> findAll()
    {
        ArrayList<POIDB> poidb = new ArrayList<POIDB>();
        //POIDB poidb = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POIDB", new String[] { } );
        System.out.println("HERE31");
        //System.out.println(cursor.moveToFirst());
        if (cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
            POIDB p = new POIDB
                    (cursor.getLong(cursor.getColumnIndex("Id")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getString(cursor.getColumnIndex("type")),
                            cursor.getString(cursor.getColumnIndex("country")),
                            cursor.getString(cursor.getColumnIndex("region")),
                            cursor.getString(cursor.getColumnIndex("description")),
                            cursor.getDouble(cursor.getColumnIndex("lon")),
                            cursor.getDouble(cursor.getColumnIndex("lat")),
                            cursor.getDouble(cursor.getColumnIndex("northing")),
                            cursor.getDouble(cursor.getColumnIndex("easting")));
            System.out.println("HERE32");
            poidb.add(p);
            cursor.moveToNext();
            }
        }
        cursor.close();
        return poidb;
    }

    public ArrayList<POIDB> findMultiple(String search)
    {
        ArrayList<POIDB> poidb = new ArrayList<POIDB>();
        //POIDB poidb = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POIDB WHERE "+search, null);
        //System.out.println(cursor.moveToFirst());
        if (cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
                POIDB p = new POIDB
                        (cursor.getLong(cursor.getColumnIndex("Id")),
                                cursor.getString(cursor.getColumnIndex("name")),
                                cursor.getString(cursor.getColumnIndex("type")),
                                cursor.getString(cursor.getColumnIndex("country")),
                                cursor.getString(cursor.getColumnIndex("region")),
                                cursor.getString(cursor.getColumnIndex("description")),
                                cursor.getDouble(cursor.getColumnIndex("lon")),
                                cursor.getDouble(cursor.getColumnIndex("lat")),
                                cursor.getDouble(cursor.getColumnIndex("northing")),
                                cursor.getDouble(cursor.getColumnIndex("easting")));
                System.out.println("HERE32");
                poidb.add(p);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return poidb;
    }

    public POIDB findId(String id)
    {
        //ArrayList<Music> music = new ArrayList<Music>();
        POIDB poidb = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POIDB WHERE Id=?", new String[] { String.valueOf(id) } );
        //System.out.println("HERE");
        //System.out.println(cursor.moveToFirst());
        if (cursor.moveToFirst())
        {
            //while(!cursor.isAfterLast())
            //{
                poidb = new POIDB
                        (cursor.getLong(cursor.getColumnIndex("Id")),
                                cursor.getString(cursor.getColumnIndex("name")),
                                cursor.getString(cursor.getColumnIndex("type")),
                                cursor.getString(cursor.getColumnIndex("country")),
                                cursor.getString(cursor.getColumnIndex("region")),
                                cursor.getString(cursor.getColumnIndex("description")),
                                cursor.getDouble(cursor.getColumnIndex("lon")),
                                cursor.getDouble(cursor.getColumnIndex("lat")),
                                cursor.getDouble(cursor.getColumnIndex("northing")),
                                cursor.getDouble(cursor.getColumnIndex("easting")));
            //System.out.println("HERE2");
                //music.add(m);
                //cursor.moveToNext();
            //}
        }
        cursor.close();
        return poidb;
    }



    public long insertRecord(long Id, String Name, String Type, String Country, String Region, String Description, double Lon, double Lat, double Northing, double Easting)
    {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement
                ("INSERT INTO POIDB(Id,Name,Type,Country,Region,Description,Lon,Lat,Northing,Easting) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        stmt.bindLong (1, Id);
        stmt.bindString (2, Name);
        stmt.bindString (3, Type);
        stmt.bindString (4, Country);
        stmt.bindString (5, Region);
        stmt.bindString (6, Description);
        stmt.bindDouble (7, Lon);
        stmt.bindDouble (8, Lat);
        stmt.bindDouble (9, Northing);
        stmt.bindDouble (10, Easting);
        //long id = stmt.executeInsert();
        stmt.executeInsert();
        return Id;
    }



    /*public int updateRecord(long Id, String Name, String Type, String Country, String Region, String Description, double Lon, double Lat, double Northing, double Easting)
    {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement
                ("UPDATE Music SET Title=?, Artist=?, Year=? WHERE Id=?");
        stmt.bindString (1, Title);
        stmt.bindString (2, Artist);
        stmt.bindLong (3, Year);
        stmt.bindLong (4, Id);
        int nAffectedRows = stmt.executeUpdateDelete();
        return nAffectedRows;
    }*/

    public int deleteRecord(long Id)
    {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM POIDB WHERE Id=?");
        stmt.bindLong (1, Id);
        int nAffectedRows = stmt.executeUpdateDelete();
        return nAffectedRows;
    }

}
