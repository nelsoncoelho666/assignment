package com.example.nelsoncoelho.finalproject;
public class OSGBProjection{
	
	public Point project (Point lonLat)
	{
		LatLon ll=new LatLon(lonLat.y,lonLat.x);
		ll.toOSGB36();
		OSRef gr = ll.toOSRef();
		return new Point(gr.getEasting(), gr.getNorthing(), lonLat.z);
		
	}

	public Point unproject (Point projected)
	{
		OSRef gr = new OSRef(projected.x,projected.y);
		LatLon ll = gr.toLatLng();
		ll.toWGS84();
		return new Point(ll.getLon(), ll.getLat(), projected.z);
	}
	
	public String getID()
	{
		return "epsg:27700";
	}
}