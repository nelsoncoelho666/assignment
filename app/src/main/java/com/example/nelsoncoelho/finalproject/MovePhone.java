package com.example.nelsoncoelho.finalproject;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.SurfaceView;
import android.widget.TextView;

/**
 * Created by nelsoncoelho on 23/03/2017.
 */

public class MovePhone extends Activity implements SensorEventListener {
    float[] accelerometerValues = new float[3];
    float[] magneticFieldValues = new float[3];
    float[] orientations = new float[3];
    float[] remappedMatrix = new float[16];
    public static volatile float k = (float)0.05;
    Sensor accel;
    Sensor magn;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SensorManager sMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accel = sMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magn = sMgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sMgr.registerListener(this,accel, SensorManager.SENSOR_DELAY_UI);
        sMgr.registerListener(this,magn, SensorManager.SENSOR_DELAY_UI);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tv_x = (TextView) findViewById(R.id.x);
        TextView tv_y = (TextView) findViewById(R.id.y);
        TextView tv_z = (TextView) findViewById(R.id.z);

        //tv_x.setText(String.valueOf(event.values[0]));
        //tv_y.setText(String.valueOf(event.values[1]));
        //tv_z.setText(String.valueOf(event.values[2]));
        //System.out.println(event.values[2]);

        if (event.sensor == accel) {
            for (int i = 0; i < 3; i++)
                //accelerometerValues[i] = event.values[i]; NORMAL WAY
                accelerometerValues[i] = (float) ((event.values[i] * k) + (accelerometerValues[i] * (1.0 - k))); //SMOOTHERED WAY
        } else {
            for (int i = 0; i < 3; i++)
                magneticFieldValues[i] = (float) ((event.values[i] * k) + (magneticFieldValues[i] * (1.0 - k)));
            //event.sensor = accelerometerValues[];
        }

        //float stored_value = raw_value*k + previous_value*(1-k)


        float[] orientationMatrix = new float[16];
        //float[] inclinationMatrix = null;


        SensorManager.getRotationMatrix(orientationMatrix, null, accelerometerValues, magneticFieldValues);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            SensorManager.remapCoordinateSystem(orientationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, remappedMatrix);
            SensorManager.getOrientation(remappedMatrix, orientations);
        }
        else
        {
            SensorManager.getOrientation(orientationMatrix, orientations);
        }

        tv_x.setText(String.valueOf(orientations[0] * 180 / Math.PI));
        tv_y.setText(String.valueOf(orientations[1] * 180 / Math.PI));
        tv_z.setText(String.valueOf(orientations[2] * 180 / Math.PI));


        //openglview.rotate((float) (orientations[2] * 180 / Math.PI));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public class OpenGLView extends SurfaceView implements Runnable
    {
        Thread outThread = null;
        public OpenGLView(Context context) {
            super(context);
        }

        @Override
        public void run() {

        }
    }
}
