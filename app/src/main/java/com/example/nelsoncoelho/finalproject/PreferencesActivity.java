package com.example.nelsoncoelho.finalproject;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by nelsoncoelho on 25/10/2016.
 */

public class PreferencesActivity extends PreferenceActivity {
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }



}
