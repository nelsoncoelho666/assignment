package com.example.nelsoncoelho.finalproject;

/**
 * Created by nelsoncoelho on 24/03/2017.
 */

public class POI {
    public double Id, Latitude, Longitude, North, South, East, West;
    public String Type;
    public POI(double Id, double Latitude, double Longitude, double North, double South, double East, double West, String Type)
    {
        this.Id = Id;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.North = North;
        this.South = South;
        this.East = East;
        this.West = West;
        this.Type = Type;
    }
}
