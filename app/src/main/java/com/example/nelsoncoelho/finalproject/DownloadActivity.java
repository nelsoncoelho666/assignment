package com.example.nelsoncoelho.finalproject;

/**
 * Created by nelsoncoelho on 08/11/2016.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class DownloadActivity extends ListActivity implements View.OnClickListener{

    String[] names, details;
    class MyTask extends AsyncTask<String,Void,String>
    {
        public String doInBackground(String... artist)
        {
            HttpURLConnection conn = null;
            try
            {
                //URL url = new URL("http://www.free-map.org.uk/course/mad/ws/hits.php?artist="+artist[0]+"&format=json");
                URL url = new URL("http://www.free-map.org.uk/course/msc/ws/poi.php?bbox=400000,100000,500000,200000");
                conn = (HttpURLConnection) url.openConnection();
                InputStream in = conn.getInputStream();
                if(conn.getResponseCode() == 200)
                {
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String result = "", line;
                    while((line = br.readLine()) !=null)
                        result += line;




                    return result;

                }
                else
                    return "HTTP ERROR: " + conn.getResponseCode();


            }
            catch(IOException e)
            {
                return e.toString();
            }
            finally
            {
                if(conn!=null)
                    conn.disconnect();
            }
        }

        public void onPostExecute(String result)
        {
            //EditText et1 = (EditText)findViewById(R.id.et1);
            //et1.setText(result);
            try
            {
            JSONArray jsonArr = new JSONArray (result);
            String text;
                text = "";
            for(int i=0; i<jsonArr.length(); i++)
            {
                JSONObject curObj = jsonArr.getJSONObject(i);
                String ID = curObj.getString("ID");
                String name = curObj.getString("name");
                String type = curObj.getString("type");
                String country= curObj.getString("country");
                String region = curObj.getString("region");
                String description = curObj.getString("description");
                String lon = curObj.getString("lon");
                String lat = curObj.getString("lat");
                String easting = curObj.getString("easting");
                String northing = curObj.getString("northing");

                text +=" ID= "+ ID + " Name = " + name + " Type = " + type + " Country = " + country + " Region = " + region +  " Description = " + description + " Lon = " + lon + " Lat = " + lat + " Easting = " + easting +" Northing = " + northing + "\n";
            }


            TextView tv = (TextView) findViewById((R.id.tv));
                tv.setText(text);
                // tv.setText(result);

            }
            catch (JSONException e)
            {
                new AlertDialog.Builder(DownloadActivity.this).setMessage(e.toString()).setPositiveButton("OK", null).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poientry);
        Button go = (Button)findViewById(R.id.go);
        go.setOnClickListener(this);

        names = new String[] { "The Crown", "The Cowherds", "The Two Brothers", "Piccolo Mondo" };
        details = new String[] { "pub, 2.5 miles north", "pub, 1.5 miles north",
                "pub, 3.5 miles northeast" , "Italian restaurant, 0.5 miles west" };
        MyAdapter adapter = new MyAdapter();
        setListAdapter(adapter);
    }

    public void onClick(View v)
    {
        EditText et1 = (EditText)findViewById(R.id.et1);
        MyTask t = new MyTask();
        t.execute(et1.getText().toString());
    }

    public void onListItemClick(ListView lv, View view, int index, long id)
    {
        // handle list item selection
    }

    public class MyAdapter extends ArrayAdapter<String>
    {
        public MyAdapter()
        {
            // We have to use ExampleListActivity.this to refer to the outer class (the activity)
            super(DownloadActivity.this, android.R.layout.simple_list_item_1, names);
        }

        public View getView(int index, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if(view==null)
            {
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.poientry, parent, false);
            }
            TextView title = (TextView)view.findViewById(R.id.poi_name), detail =
                    (TextView)view.findViewById(R.id.poi_desc);
            title.setText(names[index]);
            detail.setText(details[index]);
            return view;
        }
    }

/*
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == R.id.upload)
        {
            // react to the menu item being selected...
            Intent intent = new Intent(this,UploadActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }*/
}


