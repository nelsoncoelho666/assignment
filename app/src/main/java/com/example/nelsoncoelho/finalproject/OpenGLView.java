package com.example.nelsoncoelho.finalproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class OpenGLView extends GLSurfaceView implements GLSurfaceView.Renderer {

    FloatBuffer vbuf;
    ShortBuffer indexbuffer;
    SurfaceTexture cameraTexture;
    GPUInterface textureInterface;
    GPUInterface gpui;
    ArrayList<Box> PointsOfInterest  = new ArrayList<Box>();
    ArrayList<POIDB> found = new ArrayList<POIDB>();

    //Box box1;
    //Box box2;
    int test = 0;

    CameraCapturer cameraCapturer;
    float[] modelview = new float[16];
    float[] perspective = new float[16];
    float[] camera = {0, 0, 0};
    float[] camera_change = {0,0,0};
    float[] rotation = {0, 0, 0};
    float inc = 0;


    public OpenGLView(Context ctx){
        super(ctx);
        setEGLContextClientVersion(2);
        setRenderer(this);
        Matrix.setIdentityM(modelview, 0);
        Matrix.setIdentityM(perspective, 0);
    }
    public OpenGLView (Context ctx, AttributeSet as) {
        super (ctx, as);
        setEGLContextClientVersion(2);
        setRenderer(this);
        Matrix.setIdentityM(modelview, 0);
        Matrix.setIdentityM(perspective, 0);
    }

    public void onSurfaceChanged(GL10 unused, int w, int h)
    {

        float hFov = 40.0f;
        GLES20.glViewport(0, 0, w, h);
        float aspectRatio = (float)w / h;
        Matrix.perspectiveM(perspective, 0, hFov / aspectRatio, aspectRatio, 0.1f, 100);
    }



    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        int[] textureId = new int[1];


        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClearDepthf(1.0f);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        final String texVertexShader =
                "attribute vec4 aVertex;\n" +
                        "varying vec2 vTextureValue;\n" +
                        "void main (void)\n" +
                        "{\n" +
                        "gl_Position = aVertex;\n" +
                        "vTextureValue = vec2(0.5*(1.0 + aVertex.x), 0.5*(1.0 - aVertex.y));\n" +
                        "}\n",
                texFragmentShader =
                        "#extension GL_OES_EGL_image_external: require\n" +
                                "precision mediump float;\n" +
                                "varying vec2 vTextureValue;\n" +
                                "uniform samplerExternalOES uTexture;\n" +
                                "void main(void)\n" +
                                "{\n" +
                                "gl_FragColor = texture2D(uTexture,vTextureValue);\n" +
                                "}\n";


        final String vertexShader =
                "attribute vec4 aVertex, aColour;\n" +
                        "varying vec4 vColour;\n" +
                        "uniform mat4 uPerspMtx, uMvMtx;\n" +
                        "void main(void)\n" +
                        "{\n"+
                        "gl_Position = uPerspMtx * uMvMtx * aVertex;\n" +
                        "vColour = aColour;\n" +
                        "}\n",
                fragmentShader =
                        "precision mediump float;\n" +
                                "varying vec4 vColour;\n" +
                                "void main(void)\n"+
                                "{\n"+
                                "gl_FragColor = vColour;\n" +
                                "}\n";



        //float[] vertices = { x1,y1,z1, x2,y2,z2, ... , xn,yn,zn };
        //float[] vertices = {0,0,0, 1,0,0, 0.5f, 1, 0 , 0,0,0, -1,0,0, -0.5f, 1, 0};
        float[] vertices = { -1,-1,0, 1,-1,0, 1,1,0, -1,-1,0, 1,1,0, -1,1,0};

        ByteBuffer vbuf0 = ByteBuffer.allocateDirect(vertices.length * Float.SIZE);
        vbuf0.order(ByteOrder.nativeOrder());
        vbuf = vbuf0.asFloatBuffer();
        vbuf.put(vertices);
        vbuf.position(0);

        short[] indices = { 0,1,2, 2,3,0 };
        ByteBuffer ibuf = ByteBuffer.allocateDirect(indices.length*Short.SIZE);
        ibuf.order(ByteOrder.nativeOrder());
        indexbuffer = ibuf.asShortBuffer();
        indexbuffer.put(indices);
        indexbuffer.position(0);

        // REF for more detail: http://stackoverflow.com/questions/6414003/using-surfacetexture-in-android
        final int GL_TEXTURE_EXTERNAL_OES = 0x8d65;

        GLES20.glGenTextures(1, textureId, 0);

        if(textureId[0] != 0)
        {
            GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, textureId[0]);
            GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES,GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES,GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

            cameraTexture = new SurfaceTexture(textureId[0]);
        }

        textureInterface = new GPUInterface(texVertexShader, texFragmentShader);
        gpui = new GPUInterface(vertexShader, fragmentShader);

        //Activates the texture unit
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        //Binds the texture to an ID
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId[0]);

        textureInterface.setUniform1I("uTexture", 0);

        cameraCapturer = new CameraCapturer();
        onResume();



        MyHelper db = new MyHelper(this.getContext());
        found = db.findAll();
        //ArrayList<POIDB> found = db.findMultiple(search);
        System.out.println("RESULT FOUND: "+found);
        int i = 0;
        for (i = 0; i < found.size(); i++) {
            try {
                POIDB row = found.get(i);
                //newPOI((float) row.northing, (float) row.easting, 1, row.type);
            } catch (IndexOutOfBoundsException e) {
                System.out.println(e.toString());
            }
        }



        /*float x = 441712;
        float x = 0;
        float y = 0;
        float z = -5;

        PointsOfInterest.add(new Box(x, y, z, gpui));*/
        newPOI(0, 0, -5, "city");


        /*x = 441705;
        //x = 0;
        y = 0;
        z = -5;*/
        //PointsOfInterest.add(new Box(x, y, z, gpui));

    }

    public void setRotationMatrix(float[] rotationMatrix){
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        gpui.select();
        modelview  = rotationMatrix.clone();
        //Matrix.translateM(modelview, 0, -camera[0], -camera[1], -camera[2]);
    }

    public void newPOI (float x, float y, float z, String type)
    {
        PointsOfInterest.add(new Box(x, y, z, type, gpui));
    }

    public void rotate(float new_rotation[])
    {
        //if(rotation > -180 || rotation < 180) {
        rotation[0] = new_rotation[0];
        rotation[1] = new_rotation[1];
        rotation[2] = new_rotation[2];

        //rotation = new_rotation;

        //Matrix.setIdentityM(modelview, 0);
        //Matrix.multiplyMM(modelview, 0, new_rotation, 0, modelview, 0);

        //gpui.sendMatrix("uMvMtx", modelview);
        //rotation = new_rotation;
        //}
    }

    public void translate (Point point)
    {
        camera[0] = (float) point.x;
        camera[1] = 0;
        camera[2] = (float) point.y;
        //camera_change[0] = camera[0];
    }

    public void onDrawFrame(GL10 unused) {
        Matrix.setIdentityM(modelview, 0);

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);


        cameraTexture.updateTexImage();



        /*gpui.setUniform4fv("uColour", new float[]{1.0f, 0.0f, 0.0f, 1.0f});
        gpui.drawBufferedData(vbuf, 0, "aVertex", 0, 3);

        gpui.setUniform4fv("uColour", new float[]{1.0f, 1.0f, 0.0f, 1.0f});
        gpui.drawBufferedData(vbuf, 0, "aVertex", 3, 3);



        Matrix.translateM(modelview, 0, -1, 0, 0);
        gpui.sendMatrix("uMvMtx", modelview);

        gpui.setUniform4fv("uColour", new float[]{0.0f, 0.0f, 1.0f, 1.0f});
        gpui.drawBufferedData(vbuf, 0, "aVertex", 3, 3);

        Matrix.rotateM(modelview, 0, 45, 0, 0, 1);
        gpui.sendMatrix("uMvMtx", modelview);

        gpui.setUniform4fv("uColour", new float[]{0.0f, 1.0f, 0.0f, 1.0f});
        gpui.drawBufferedData(vbuf, 0, "aVertex", 3, 3);*/

        textureInterface.select();

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        textureInterface.drawBufferedData(vbuf, 0, "aVertex", 0, 6);


        GLES20.glEnable(GLES20.GL_DEPTH_TEST);


        gpui.select();
        gpui.sendMatrix("uPerspMtx" ,perspective);
        Matrix.setIdentityM(modelview, 0);


        Matrix.rotateM(modelview, 0, rotation[1], 1, 0, 0);
        Matrix.rotateM(modelview, 0, -rotation[2], 0, 1, 0);
        //Matrix.rotateM(modelview, 0, 90, 1, 0, 0);
        //Matrix.rotateM(modelview, 0, rotation[0], 0, 1, 0);




        //TRANSLATE REAL COORDINATES TO WORLD COORDINATES
        //Matrix.translateM(modelview, 0, -camera[0], -camera[1], -camera[2]);
        //Matrix.translateM(modelview, 0, 0, -10, -5);

        gpui.sendMatrix("uMvMtx", modelview);




        //Matrix.setIdentityM(modelview, 0);
        //Matrix.rotateM(modelview, 0, rotation[1], 1, 0, 0);
        //gpui.sendMatrix("uMvMtx", modelview);


        for(int i = 0; i < PointsOfInterest.size(); i++) {
            PointsOfInterest.get(i).render(gpui);
        }
        //box2.render(gpui);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int X = (int) event.getX();
        int Y = (int) event.getY();
        int eventaction = event.getAction();
        boolean found = false;
        for(int i = 0; i < PointsOfInterest.size(); i++)
        {
            /*if(((camera[0] + X) >= (PointsOfInterest.get(i).x - 10))  && ((camera[0] + X) <= (PointsOfInterest.get(i).x + 10)))
                found = true;


            if(((camera[2] + Y) >= (PointsOfInterest.get(i).y - 10))  && ((camera[2] + Y) <= (PointsOfInterest.get(i).y + 10)))
                found = true;*/

            if((X >= (PointsOfInterest.get(i).x)) && ((X <= (PointsOfInterest.get(i).x + 1800))))
                if((Y >= (PointsOfInterest.get(i).y)) && ((Y <= (PointsOfInterest.get(i).y + 1800))))
                    found = true;

            if(found) {
                System.out.println(PointsOfInterest.get(i).x + " was FOUND");
                Toast.makeText
                        (this.getContext(), this.found.get(i).name+" ("+this.found.get(i).type+")", Toast.LENGTH_LONG).show();
            }
            else
                System.out.println(PointsOfInterest.get(i).x+" was NOT FOUND");
        }
        switch (eventaction) {
            case MotionEvent.ACTION_DOWN:
                //Toast.makeText(this, "ACTION_DOWN AT COORDS "+"X: "+X+" Y: "+Y, Toast.LENGTH_SHORT).show();
                //isTouch = true;
                break;

            case MotionEvent.ACTION_MOVE:
                //Toast.makeText(this, "MOVE "+"X: "+X+" Y: "+Y, Toast.LENGTH_SHORT).show();
                break;

            case MotionEvent.ACTION_UP:
                //Toast.makeText(this, "ACTION_UP "+"X: "+X+" Y: "+Y, Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    public void onResume()
    {
        if(cameraCapturer != null) {
            cameraCapturer.openCamera();
            try {
                cameraCapturer.startPreview(cameraTexture);
            } catch (IOException e) {
                cameraCapturer.releaseCamera();
            }
        }
    }

    public void onPause(){
        cameraCapturer.releaseCamera();
    }


    public void plusX() {
        camera[0]++;
    }

    public void minusX() {
        camera[0]--;
    }

    public void plusY() {
        camera[1]++;
    }

    public void minusY() {
        camera[1]--;
    }

    public void plusZ() {
        camera[2]++;
    }

    public void minusZ() {
        camera[2]--;
    }

}